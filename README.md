# 953323 2/2019 Term Project
## Activity Registration System (Frontend)
For the backend, please visit the below link

<https://gitlab.com/somruk99/953323-2-2019-term-project-5023-backend>
## Features implemented

- The administrator can create activities in each semester
The activity information consists of the leader who teaches the activity (There can be one leader in each activity), the activity name and id, semester which open and the academic year, the maximum number of students who can enroll, and the number of credit of the hour attend
- The students can enrolled in the activity. The students cannot enroll more than 50 hours in each semester.
- The students can see which activity they have enrolled.
- The students can remove the activity they have enrolled in.
- The students can see how much they get the activity score. The activity hours are calculated by 2 points per hour

## Creator

Somruk Laothamjinda 602115023 

somruk_laothamjinda@elearning.cmu.ac.th

## Lecturer

Jayakrit Hirisajja

## How to run
1.) Launch Git and make sure that node.js and npm are installed

2.) Run the below command to install the modules used by the project
```
npm install
```
3.) Run the below command to launch the web frontend, then `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
```
ng s -o
```
Note: Please make sure the backend is also running since the web frontend needs data from the backend to work

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.0.