import { Injectable } from '@angular/core';
import Teacher from '../entity/teacher';
import { TeacherService } from './teacher.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class TeacherImplService extends TeacherService {
  getTeachers(): Observable<Teacher[]> {
    return this.http.get<Teacher[]>(environment.teacherApi);
  }
  getTeacher(id: number): Observable<Teacher> {
    return this.http.get<Teacher>(environment.teacherApi+"/"+id);
  }
  getTeacherByEmailAndPassword(email: string, password: string): Observable<Teacher>{
    throw new Error("Method not implemented.");
  }

  constructor(private http: HttpClient) { super() }
}

