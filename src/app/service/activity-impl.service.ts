import { Injectable } from '@angular/core';
import { ActivityService } from './activity.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import Activity from '../entity/activity';
import Teacher from '../entity/teacher';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ActivityImplService extends ActivityService {

  constructor(private http: HttpClient) { super(); }
  
  getActivityList(): Observable<Activity[]> {
    return this.http.get<Activity[]>(environment.activityApi);
  }
  getActivity(id: number): Observable<Activity> {
    return this.http.get<Activity>(environment.activityApi +"/"+id);
  }
  saveActivity(student: Activity): Observable<Activity> {
    return this.http.post<Activity>(environment.activityApi,student);
  }
  saveEnrollActivity(id: number) {
    throw new Error("Method not implemented.");
  }
  check(id: number): boolean {
    throw new Error("Method not implemented.");
  }
  getEnrollActivity(): Observable<Activity[]> {
    throw new Error("Method not implemented.");
  }
  saveEdit(activity: Activity): Observable<Activity> {
    return this.http.put<Activity>(environment.activityApi+"/"+activity.id,activity);
  }
  saveUpdateActivity(id: number) {
    throw new Error("Method not implemented.");
  }
  getEnrolledStudentsIdInActivity(id: number): number[] {
    throw new Error("Method not implemented.");
  }
  getTeacherInActivity(id: number): Observable<Teacher[]> {
    throw new Error("Method not implemented.");
  }
  getActivityListByTeacherId(id: number): Observable<Activity[]>  {
    return this.http.get<Activity[]>(environment.activityApi+"/teacher/"+id);
  }

}
