import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
     static profile_validation_messages = {
    'studentId': [
      { type: 'required', message: 'Student Id is required'},
      { type: 'pattern', message: 'Student Id must contain 9 digits of 0-9'}
    ],
    'name': [
      {type: 'required', message: 'Please enter your name'},
      {type: 'pattern', message: 'Name must contains 2 to 30 characters'}
    ],
    'surname': [
      { type: 'required', message: 'Please enter your surname'},
      {type: 'pattern', message: 'Surname must contains 2 to 30 characters'}
    ],
    'image': [],
    'birth': [
      { type: 'required', message: 'Please enter your birthday'},
      { type: 'pattern', message: 'Birthday should be in dd/mm/yyyy'}
    ],
    'email': [
      { type: 'required', message: 'Please enter your email'},
      { type: 'pattern', message: 'Email not in a correct format'}
    ],
    'password': [
      { type: 'required', message: 'Please enter your password'},
      { type: 'pattern', message: 'Password must contains 5 to 15 characters'}
    ],
    'confirmPassword': [
      { type: 'required', message: 'Please enter your password'},
      { type: 'mustMatch', message: 'Passwords do not match'}
    ]
  };
  

  constructor() { }
}
