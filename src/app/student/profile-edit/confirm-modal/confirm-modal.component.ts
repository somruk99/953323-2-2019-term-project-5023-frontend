import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
export interface DialogData {
  username:string;
  changePassword:boolean;
}
@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.css']
})
export class ConfirmModalComponent implements OnInit {
  
  constructor(
    public dialogRef: MatDialogRef<ConfirmModalComponent>, 
    private router: Router,
    private http: HttpClient,
    private fb:FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {}
  ngOnInit(): void {
    this.setupForm();
  }
  hide:boolean;
  newPassword:string;
  confirmNewPassword:string;
  setupForm() {
    console.log(this.data.changePassword);
    if (!this.data.changePassword) {
      this.title = "Please enter your current password to save your profile";
      this.form = this.fb.group({
        password: ['']
      });
    }
    else {
      this.title = "Please enter your current and new password to save your profile";
      this.form = this.fb.group({
        password: ['',Validators.compose([Validators.required,
          Validators.pattern('^.{5,15}$')])],
          newPassword: ['',Validators.compose([Validators.required,
            Validators.pattern('^.{5,15}$')])],
        confirmNewPassword: ['',Validators.compose([Validators.required])]
      },{
          validator: this.matchPassword('newPassword', 'confirmNewPassword')
      })
    }
  }
  
  currentPassword: string;
  currentPasswordConfirm: string;
  title: string;
  form: any;

  matchPassword(password:string,confirmPassword: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[password];
    const matchingControl = formGroup.controls[confirmPassword];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
    }
    // set error on matchingControl if validation fails
    matchingControl.setErrors( control.value!==matchingControl.value? {mustMatch: true} : null);
  }
}
  validation_messages = {
    'password': [
      { type: 'required', message: 'Please enter your password'},
      { type: 'pattern', message: 'Password must contains 5 to 15 characters'}
    ],
    'newPassword': [
      { type: 'required', message: 'Please enter your new password'},
      { type: 'pattern', message: 'Password must contains 5 to 15 characters'}
    ],
    'confirmNewPassword': [
      { type: 'required', message: 'Please enter your new confirm password'},
      { type: 'pattern', message: 'Password must contains 5 to 15 characters'},
      { type: 'mustMatch', message: 'New passwords do not match'}
    ]
  };
  //boolean for unmatched password
  unmatchedPassword = false;
 
  goBack(): void {
    this.dialogRef.close();
    //this.router.navigate(['student/viewlist']);
  }
  checkMatchPassword(): any {
    const username = this.data.username;
    const password = this.form.get('password').value;
    return this.http.post<any>(
      'http://localhost:8080/verifyPassword/'+username+"/"+password,
      username,password
    )
  }
  submit() {
    this.checkMatchPassword().subscribe(
      result=> {
        if (result.match) {
          this.unmatchedPassword = false;

          if (this.data.changePassword) {
            result.newPassword = this.form.get('newPassword').value;
          }
          else {
            result.newPassword = this.form.get('password').value;
          }
          setTimeout( ()=> 
          this.dialogRef.close(result)
          ,300);
        
        }
        else {
          this.unmatchedPassword = true;
        }
      }
    )
  }

}
