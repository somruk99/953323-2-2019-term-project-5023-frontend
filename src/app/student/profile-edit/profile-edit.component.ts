import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute, Router } from '@angular/router';
import Student from '../../entity/student';
import { HttpEventType, HttpEvent } from '@angular/common/http';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthenticationService } from '../../service/authentication.service';
import { MatDialog } from '@angular/material';
import { StudentService } from '../../service/student.service';
import { FileUploadService } from '../../service/file-upload.service';
import { ConfirmModalComponent } from './confirm-modal/confirm-modal.component';
import { CommonService } from '../../common.service';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.css']
})
export class ProfileEditComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private fb:FormBuilder, 
    private router:Router, 
    private studentService: StudentService,
    private authenService: AuthenticationService,
    private fileUploadService:FileUploadService) { }

  form = this.fb.group({
    //id: [''],
    studentId: ['',Validators.compose([
      Validators.required,
      Validators.pattern('[0-9]{9}')
    ])],
    name: ['',Validators.compose([Validators.required,
      Validators.pattern('^.{2,30}$')])],
    surname: ['',Validators.compose([Validators.required,
      Validators.pattern('^.{2,30}$')])],
    image: [''],
    email: ['',Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
    password: ['']
  })

  validation_messages = CommonService.profile_validation_messages;

  minDate:Date;
  maxDate:Date;
  hide:boolean;
  //Default image
  imgsrc: string = 'assets/images/person-icon.png';
    //Preview the image link
    previewImage() {
      this.imgsrc = this.form.get('image').value;
    }
    imgChange = false;
    loading : boolean;

  matchPassword(password:string,confirmPassword: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[password];
      const matchingControl = formGroup.controls[confirmPassword];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
      }
      // set error on matchingControl if validation fails
      matchingControl.setErrors( control.value!==matchingControl.value? {mustMatch: true} : null);
    }
  }

  student : Student;

  //Boolean to check if the student wants to change password
  changePassword : boolean = false;
  changeStudentPassword() {
    this.changePassword = !this.changePassword
  }

 
  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      width: '400px',
      data : { 
        username: this.form.get('email').value, 
        changePassword: this.changePassword}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        //Do nothing lol
      }
      else {
        if (result) {
        this.form.get('password').setValue(result.newPassword);
        this.saveStudent();
      }
      }
    });
  }
  progress: number;
  uploadedUrl: string;
  //Save + attach img to student
  saveStudent() {
    
    const uploadedFile = this.selectedImgFile;
    this.progress = 0;
    if (this.imgChange) {
    this.fileUploadService.uploadFile(uploadedFile)
      .subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Sent:
            console.log('Request has been made!');
            break;
          case HttpEventType.ResponseHeader:
            console.log('Response header has been received!');
            break;
          case HttpEventType.UploadProgress:
            this.progress = Math.round(event.loaded / event.total * 100);
            console.log(`Uploaded! ${this.progress}%`);
            break;
          case HttpEventType.Response:
            console.log('User successfully created!', event.body);
            this.uploadedUrl = event.body;
            this.form.patchValue({
              image: this.uploadedUrl
            });
            console.error(this.form.value);
            this.previewImage();
            console.log(this.form.value);
            this.form.get('image').updateValueAndValidity();
            setTimeout(() => {
              this.progress = 0;
            }, 1500);
            this.studentService.saveStudentProfile(this.student.id,this.form.value).subscribe(
              (student) => {
              
                alert('Profile successfully updated!, please login again');
                this.router.navigate(['../login']);
               
              }, (error)=> { alert('There was an error updaing your information, please try later!');
            })
          }
        });
      }
      else {
        setTimeout(() => {
          this.progress = 0;
        }, 1500);
        this.studentService.saveStudentProfile(this.student.id,this.form.value).subscribe(
          (student) => {
          
            alert('Profile successfully updated!, please login again');
            this.router.navigate(['../login']);
           
          }, (error)=> { alert('There was an error updaing your information, please try later!');
        })
      }
  }
  selectedImgFile: File;
  //Preview image
  onSelectedFilesChanged(files?: FileList) {
    this.imgChange = true;
    const uploadedFile = files.item(0);
    var fileReader  = new FileReader();
    fileReader.onload = function() {
      var output = document.getElementById('previewImg');
      output.setAttribute("src",fileReader.result.toString())
    }
    fileReader.readAsDataURL(uploadedFile);
    this.selectedImgFile = uploadedFile;
  }

  duplicateStudentId: boolean;
  showStudentIdDupError:boolean;
  
  submit() {
    //If the student id is the same, then just skip to password confirmation
    if (this.form.get('studentId').value === (this.student.studentId)) {
      this.openDialog()
    }
    else {
    //Check for duplicate student id in database
    this.authenService.checkDupStudentId(this.form.value).subscribe(
      (dupId) => {

        this.duplicateStudentId = (dupId.dup)? true:false;
          //If either the email or student id is duplicated, then do not save
          if (this.duplicateStudentId) {
            this.showStudentIdDupError = true;
          }
          else {
            this.showStudentIdDupError = false;
            //Open to show profile confirm dialog and save
            this.openDialog();
          } 
          })
        }
    }
  


  getStudentId() {
    return this.authenService.getCurrentUser().id;
  }
  ngOnInit():void {
    this.loading = true;
    this.route.params
    .subscribe((params: Params) => {
      this.studentService.getStudentProfile(this.getStudentId())
      .subscribe((inputStudent: Student) => {
        this.student = inputStudent
        this.form.get('studentId').patchValue(this.student.studentId);
        this.form.get('name').patchValue(this.student.name);
        this.form.get('surname').patchValue(this.student.surname);
        this.form.get('email').patchValue(this.student.email);
        //this.form.get('password').patchValue(this.student.password);
        this.form.get('image').patchValue(this.student.image);
        this.previewImage();
        this.loading = false;
      })
    });
   
  }

}
