import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormControl, FormGroup } from '@angular/forms';
import Teacher from '../../entity/teacher';
import Activity from '../../entity/activity';
import { MatPaginator, MatSort, MatTable, MatDialog } from '@angular/material';
import { ActivityService } from '../../service/activity.service';
import { AuthenticationService } from '../../service/authentication.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { FindActivityDatasource } from './find-activity-datasource';
import { environment } from '../../../environments/environment';
import { FindConfirmModalComponent } from './find-confirm-modal/find-confirm-modal.component';
import { ActivityInfoModalComponent } from '../../shared/activity-info-modal/activity-info-modal.component';

@Component({
  selector: 'app-find-activity',
  templateUrl: './find-activity.component.html',
  styleUrls: ['./find-activity.component.css']
})
export class FindActivityComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: FindActivityDatasource;
 
  displayedColumns = ['id', 'name', 'location','hostTeacher','hourCredit','semesterAndAcademicYear','amount','info','enroll'];
  acivities: Activity[];
  filter: string;
  filter$: BehaviorSubject<any>;
  activity: Activity;
  loading:boolean;

  enrolledAmt: number;
  enrolledHour: number;

  constructor(
    private activityService: ActivityService, 
    private authenService: AuthenticationService,
    private http: HttpClient,
    private router: Router,
    public dialog: MatDialog) { }
  hostTeachers: Teacher[];
  studentId : number;
  ngOnInit() {
    this.studentId = this.authenService.getCurrentUser().id;
  }
  // Date Filter 
  getDateRange(value) {
    // getting date from calendar
    const fromDate = value.fromDate;
    const toDate = value.toDate;

    console.log(fromDate, toDate);
    this.applyDateFilter(fromDate, toDate);
  }

  pipe: DatePipe;

  filterForm = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
    name: new FormControl()
  });
  filterResult(value) {
    // getting date from calendar
    const fromDate = value.fromDate;
    const toDate = value.toDate;
    const name = value.name;

    console.log(fromDate, toDate,name);
    this.applyBigFilter(fromDate, toDate,name);
  }
  reset() {
    this.filterForm.get('fromDate').setValue(null);
    this.filterForm.get('toDate').setValue(null);
    this.filterForm.get('name').setValue('');
    this.applyBigFilter(null,null,'');
  }
  applyBigFilter(startDate : Date, endDate: Date, name:string) {
    console.log(startDate,endDate);
    if (name =='' || name==null) {
      name = '';
    }
    //No date
    if(startDate == ( null ) && endDate == ( null )) { 
        this.applyFilter(name); 
    }
    //Only start date
    else if(startDate == ( null ) && endDate != ( null )) {
      this.filter$.next([0,endDate,name]);
    }
    //Only end date
    else if(startDate != ( null ) && endDate == ( null )) {
      this.filter$.next([startDate,Infinity,name]);
    }
    //Both
    else {
      this.filter$.next([startDate,endDate,name]);
    }
  }


  get fromDate() { return this.filterForm.get('fromDate'); }
  get toDate() { return this.filterForm.get('toDate'); }


  applyDateFilter(startDate : Date, endDate: Date) {
    
    //No date
    console.log(startDate,endDate);
    if(startDate == ( null ) && endDate == ( null )) { 
      this.applyFilter(''); 
    }
    //Only start date
    else if(startDate == ( null ) && endDate != ( null )) {
      this.filter$.next([0,endDate]);
    }
    //Only end date
    else if(startDate != ( null ) && endDate == ( null )) {
      this.filter$.next([startDate,Infinity]);
    }
    //Both
    else {
      this.filter$.next([startDate,endDate]);
    }
  }

  ngAfterViewInit() {
    this.addData();
  }

  addData() {
    this.loading = true;
    this.activityService.getActivityList()
      .subscribe(activities => {
        this.dataSource = new FindActivityDatasource();
        this.dataSource.data = activities;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
        this.table.dataSource = this.dataSource;
        this.acivities = activities;
        this.loading = false;
      })

      this.http.get<any>("http://localhost:8080/students/activities/"+this.studentId).subscribe ( (data) => {
        this.enrolledAmt = data.enrolledActivities.length;
        this.enrolledHour = 0;
        for (var i =0; i < this.enrolledAmt; i++) {
            this.enrolledHour += data.enrolledActivities[i].hourCredit;
        }
    })
  }

  containEnrolled(activity:Activity,id:number) {
      for (var i=0; i<activity.enrolledStudents.length;i++) {
        
        if (activity.enrolledStudents[i].id == id) {
         
          return true;
        }
      }
        return false;
       
  } 

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

    
  enroll(activity:Activity) {
    if (confirm("Are you sure you want to enroll into "+activity.name+"?"))  {
      this.http.get<Activity>(environment.activityApi+"/"+activity.id+"/enroll/"+this.studentId).subscribe(
        () => { this.addData(); }
      )  
    }
  }

  openInformationModal(activity: any) {
    const dialogRef = this.dialog.open(ActivityInfoModalComponent, {
      width: '700px',
      data : { 
        name: activity.name,
        location:activity.location,
        description: activity.description,
        host: activity.host.name + ' ' +  activity.host.surname,
        maxEnrolledAmount: activity.maxEnrolledStudent,
        currentEnrolled: activity.enrolledStudents.length,
        hourCredit: activity.hourCredit,
        semester: activity.semester,
        academicYear: activity.academicYear,
      }
    });
    dialogRef.afterClosed().subscribe(confirm => { 
      // Don't do anything 
    });
  }

  openEnrollConfirmDialog(activity:any) {
    const dialogRef = this.dialog.open(FindConfirmModalComponent, {
      width: '700px',
      data : { 
        name: activity.name,
        location:activity.location,
        description: activity.description,
        host: activity.host.name + ' ' +  activity.host.surname,
        maxEnrolledAmount: activity.maxEnrolledStudent,
        currentEnrolled: activity.enrolledStudents.length,
        hourCredit: activity.hourCredit,
        semester: activity.semester,
        academicYear: activity.academicYear,
        studentEnrolledHours: this.enrolledHour
      }
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.http.get<Activity>(environment.activityApi+"/"+activity.id+"/enroll/"+this.studentId).subscribe(
          () => { this.addData(); })
      }
      else {
        //Do nothing;
      }
    });
 



  }}
