import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import Teacher from '../../../entity/teacher';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
export interface EnrollConfirmData{
  name: string,
  location:string,
  description: string,
  host: string,
  maxEnrolledAmount: number,
  hourCredit:number,
  semester: number,
  academicYear: number,
  studentEnrolledHours: number,
  currentEnrolled: number
}
@Component({
  selector: 'app-find-confirm-modal',
  templateUrl: './find-confirm-modal.component.html',
  styleUrls: ['./find-confirm-modal.component.css']
})
export class FindConfirmModalComponent implements OnInit,AfterViewInit {
  
  constructor(
    public dialogRef: MatDialogRef<FindConfirmModalComponent>, 
    private fb:FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public data: EnrollConfirmData
    ) {}

  form :any;
  

  ngOnInit(): void {
    //Set up the information preview
    this.setupForm();
  }
  ngAfterViewInit(): void {
  }
  setupForm() {
    this.form = this.fb.group({
      name: [''],
      location: [''],
      description: [''],
      host: [''],
      //maxEnrolledAmount: [''],
      hourCredit: ['']//,
      //semester: [''],
      //academicEnrolledHours: ['']
    });
  }
  
  isFull() {
    if (this.data.maxEnrolledAmount == this.data.currentEnrolled) return true
    else return false;
  }
  isStudentHourMax() {
    if ( (this.data.hourCredit + this.data.studentEnrolledHours) > 50)  {
      return true
    }
    
    else return false;
  }


    
  goBack(): void { this.dialogRef.close(false); }

  submit() {
    setTimeout( ()=> 
      this.dialogRef.close(true)
      ,300); 
    }
}



