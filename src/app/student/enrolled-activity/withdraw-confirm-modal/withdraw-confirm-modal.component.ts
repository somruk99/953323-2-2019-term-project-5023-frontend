import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import Teacher from '../../../entity/teacher';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
export interface WithdrawConfirmData{
  name: string,
  location:string,
  description: string,
  host: string,
  maxEnrolledAmount: number,
  hourCredit:number,
  semester: number,
  academicYear: number,
  studentEnrolledHours: number,
  currentEnrolled: number
}
@Component({
  selector: 'app-withdraw-confirm-modal',
  templateUrl: './withdraw-confirm-modal.component.html',
  styleUrls: ['./withdraw-confirm-modal.component.css']
})
export class WithdrawConfirmModalComponent implements OnInit,AfterViewInit {
  
  constructor(
    public dialogRef: MatDialogRef<WithdrawConfirmModalComponent>, 
    private fb:FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public data: WithdrawConfirmData
    ) {}

  form :any;
  

  ngOnInit(): void {
    //Set up the information preview
    this.setupForm();
  }
  ngAfterViewInit(): void {
  }
  setupForm() {
    this.form = this.fb.group({
      name: [''],
      location: [''],
      description: [''],
      host: [''],
      hourCredit: ['']
    });
  }
  



    
  goBack(): void { this.dialogRef.close(false); }

  submit() {
    setTimeout( ()=> 
      this.dialogRef.close(true)
      ,300); 
    }
}



