import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ActivityService } from '../../service/activity.service';
import { Router } from '@angular/router';
import { MatPaginator, MatSort, MatTable, MatDialog } from '@angular/material';
import Activity from '../../entity/activity';
import { BehaviorSubject } from 'rxjs';
import { TeacherService } from '../../service/teacher.service';
import { ActivityListDataSource } from './activity-list-datasource';
import { ActivityInfoModalComponent } from '../../shared/activity-info-modal/activity-info-modal.component';

@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css']
})
export class ActivityListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: ActivityListDataSource ;
  loading:boolean;
/** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
displayedColumns = ['id', 'name', 'location','hostTeacher','hourCredit','semesterAndAcademicYear','amount','info'];
activities: Activity[];
  filter: string;
  activitySize:number;
  filter$: BehaviorSubject<string>;
  constructor(private dialog: MatDialog,private activityService: ActivityService,private teacherService: TeacherService, private router: Router) { }
  //Add data into the table
  addData() {
    this.loading = true;
    this.activityService.getActivityList()
    .subscribe(activities =>  
      setTimeout(
        () => {
      this.dataSource = new ActivityListDataSource();
      this.dataSource.data = activities;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;
      this.table.dataSource = this.dataSource;
      this.activities = activities;
      this.activitySize = activities.length;
      this.loading=false;
    }
    )
    ),0}
  ngOnInit() {
    
  }
  openInformationModal(activity: any) {
    const dialogRef = this.dialog.open(ActivityInfoModalComponent, {
      width: '700px',
      data : { 
        name: activity.name,
        location:activity.location,
        description: activity.description,
        host: activity.host.name + ' ' +  activity.host.surname,
        maxEnrolledAmount: activity.maxEnrolledStudent,
        currentEnrolled: activity.enrolledStudents.length,
        hourCredit: activity.hourCredit,
        semester: activity.semester,
        academicYear: activity.academicYear,
      }
    });
    dialogRef.afterClosed().subscribe(confirm => { 
      // Don't do anything 
    });
  }

  ngAfterViewInit() {
    this.addData();
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }
  
}
