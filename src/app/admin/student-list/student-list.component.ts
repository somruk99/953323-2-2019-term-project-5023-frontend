import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { StudentListDataSource } from './student-list-datasource';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { StudentService } from '../../service/student.service';
import Student from '../../entity/student';
import { MatPaginator, MatSort, MatTable } from '@angular/material';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements AfterViewInit,OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Student>;
  dataSource: StudentListDataSource;

/** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
displayedColumns = [
  'id',
  'studentId',
  'name',
  'surname',
  'email',
  'image'
 
];
  students: Student[];
  studentSize:number;
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(
    private studentService: StudentService,
    private http: HttpClient) { }
  getStudentProfile(): Observable<any> {
    return this.http.get<any[]>
    ("http://localhost:8080/studentUsers");
  }
  ngOnInit() {

  }
  loading:boolean;
  addData() {
    this.loading = true;
    this.getStudentProfile()
    .subscribe(students => {
      setTimeout(
        () => {
      this.dataSource = new StudentListDataSource();
      this.dataSource.data = students;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;
      this.table.dataSource = this.dataSource;
      this.students = students;
      this.loading = false;
      this.studentSize = students.length;
    },0
    )
  })
 
}

  ngAfterViewInit() {
    this.addData();
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }
}
