import { Component, OnInit } from '@angular/core';
import Teacher from '../../entity/teacher';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivityService } from '../../service/activity.service';
import { TeacherService } from '../../service/teacher.service';

@Component({
  selector: 'app-activity-create',
  templateUrl: './activity-create.component.html',
  styleUrls: ['./activity-create.component.css']
})
export class ActivityCreateComponent implements OnInit {
  constructor(private fb:FormBuilder,private router:Router,private activityService: ActivityService,private teacherService :TeacherService) {}
  form = this.fb.group({
    id: [''],
    name: ['',Validators.compose([Validators.required])],
    location: ['',Validators.compose([Validators.required])],
    description: ['',Validators.compose([Validators.required])],
    hostTeacher: [''],
    hourCredit: ['',Validators.compose([Validators.required,Validators.max(50)])],
    maxEnrolledStudent: ['',Validators.compose([Validators.required,Validators.max(100)])],
    semester: ['',Validators.compose([Validators.required])],
    academicYear: ['',Validators.compose([Validators.required])]
    
    
  })
  semester: number;
  year:number;

  loading:boolean;

   validation_messages = {
    'name': [
      {type: 'required', message: 'Please enter the activity name'}
    ],
    'location': [
      { type: 'required', message: 'Please enter the activity location'}
    ],
    'description': [
      { type: 'required', message: 'Please enter the activity description'}
    ],
    'hourCredit': [
      { type: 'required', message: 'Please enter the amount of hour of the activity'},
      { type: 'max', message: 'Hour can not exceed 50 hours!'}
    ],
    'maxEnrolledStudent': [
      { type: 'required', message: 'Please enter the max enrolled amount'},
      { type: 'max', message: 'Max amount is 100!'}
    ],
    'semester': [
      { type: 'required', message: 'Please enter the semester'}
    ],
    'academicYear': [
      { type: 'required', message: 'Please enter the academic year'}
    ],
   
  };
 
  hostId: number;

  //Available teachers to host the activity
  hostTeachers : Teacher[];
  //Must convert form to actual object, just in case
  submit() {
    var newActivity = this.form.value;
    newActivity.host = new Teacher();
    newActivity.host.id = this.hostId;
    
    this.activityService.saveActivity(newActivity).subscribe(
      (activity) => {
        alert('This activity has been added successfully!')
        
        this.router.navigate(['admin/activitylist']);
      }, (error)=> { alert('could not save value');
    })
  }

  ngOnInit() {
    this.loading= true;
    //Add teachers
    this.teacherService.getTeachers().subscribe(
      (teachers) => {
        this.hostTeachers = teachers;
        this.loading = false;
      }
    )
  }

  quickSetup() {
    this.form.patchValue({name:'Hello world'});
    this.form.patchValue({location:"CAMT"});
    this.form.patchValue({description:"No one read description"});
    this.form.patchValue({hourCredit:20});
    this.form.patchValue({maxEnrolledStudent:30});
    this.form.patchValue({academicYear:2020});

  }

}
