import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import Activity from '../../entity/activity';
import { environment } from '../../../environments/environment';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatTable, MatSort, MatPaginator, MatDialog } from '@angular/material';
import Student from '../../entity/student';
import { AssignedActivityStudentDataSource } from './assigned-activity-students-datasource';
import { BehaviorSubject } from 'rxjs';
import { StudentService } from '../../service/student.service';
import { ActivityService } from '../../service/activity.service';
import { HttpClient } from '@angular/common/http';
import { RejectConfirmModalComponent } from './assigned-activity-students/reject-confirm-modal/reject-confirm-modal.component';

@Component({
  selector: 'app-assigned-activity-students',
  templateUrl: './assigned-activity-students.component.html',
  styleUrls: ['./assigned-activity-students.component.css']
})
export class AssignedActivityStudentsComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Student>;
  dataSource: AssignedActivityStudentDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'studentId', 'name', 'surname','image', 'reject'];
  loading: boolean;
  activity: Activity;
  enrolled: number;
  maxEnrolled: number;
  
    activityName: string;
  
    filter: string;
    filter$: BehaviorSubject<string>;

    constructor(
      private studentService: StudentService,
      private activityService: ActivityService,
      private route: ActivatedRoute,
      private router: Router,
      private http: HttpClient,
      private dialog: MatDialog
      ) { }

      addData() {
        this.loading = true;
        this.route.params
        .subscribe((params: Params) => {
          
        this.activityService.getActivity(+params['id'])
        .subscribe(activity => {
          setTimeout(
            () => {
          this.dataSource = new AssignedActivityStudentDataSource();
          this.dataSource.data = activity.enrolledStudents;
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.filter$ = new BehaviorSubject<string>('');
          this.dataSource.filter$ = this.filter$;
          this.table.dataSource = this.dataSource; 
          this.activity = activity;
          this.loading = false;
          this.activityName = activity.name;
          this.enrolled = activity.enrolledStudents.length;
          this.maxEnrolled = activity.maxEnrolledStudent;
        },0)}
        )
      }
      )}
    
      ngOnInit(): void {
      
      }
  
    ngAfterViewInit() {
      this.addData();
    }
    applyFilter(filterValue: string) {
      this.filter$.next(filterValue.trim().toLowerCase());
    }

    isEnrolled(studentId:number): boolean {
      for (var student of this.activity.enrolledStudents) {
        if (student.id == studentId) {
          return true;
        }
      }
      return false;
    }

    withdraw(activity:Activity,studentId:number) {
        {
        this.http.get<Activity>(environment.activityApi+"/"+activity.id+"/withdraw/"+studentId).subscribe(
          () => { this.addData(); }
        )  
      }
    }

    openRejectConfirmDialog(activity:any,student :Student) {
      const dialogRef = this.dialog.open(RejectConfirmModalComponent, {
        width: '700px',
        data : { 
          name: activity.name,
          studentName: student.name +' '+ student.surname,
          studentId: student.id,
          studentImg: student.image
        }
      });
      dialogRef.afterClosed().subscribe(confirm => {
        if (confirm) {
          this.withdraw(activity,student.id)
        }
        else {
          //Do nothing;
        }
      });
  
    }
    
    //Go back to activity list page
    backtoActivityListPage(){
      this.router.navigate(['teacher/assignedactivitylist']);
    }
  
  
  }
