import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder } from '@angular/forms';
export interface RejectConfirmData{
  name: string,
  studentName: string,
  studentId: number,
  studentImg: string
}
@Component({
  selector: 'app-reject-confirm-modal',
  templateUrl: './reject-confirm-modal.component.html',
  styleUrls: ['./reject-confirm-modal.component.css']
})
export class RejectConfirmModalComponent implements OnInit,AfterViewInit {
  
  constructor(
    public dialogRef: MatDialogRef<RejectConfirmModalComponent>, 
    private fb:FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public data: RejectConfirmData
    ) {}

  form :any;
  imgsrc: string = 'assets/images/person-icon.png';
  
  ngOnInit(): void {
    //Set up the information preview
    this.setupForm();
    this.imgsrc = this.data.studentImg;
  }
  ngAfterViewInit(): void {
  }
  setupForm() {
    this.form = this.fb.group({
      name: [''],
      studentName: [''],
      studentId: [''],
      studentImg: ['']

    });
  }
     
  goBack(): void { this.dialogRef.close(false); }

  submit() {
    setTimeout( ()=> 
      this.dialogRef.close(true)
      ,300); 
    }
}
