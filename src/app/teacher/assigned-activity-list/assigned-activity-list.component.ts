import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';

import { BehaviorSubject } from 'rxjs';
import Activity from '../../entity/activity';
import { ActivityService } from '../../service/activity.service';
import { RouterLink, Router } from '@angular/router';
import { TeacherService } from '../../service/teacher.service';

import { FormGroup, FormControl } from '@angular/forms';
import { AuthenticationService } from '../../service/authentication.service';
import { AssignedActivityListDataSource } from './assigned-activity-list-datasource';
import { ActivityInfoModalComponent } from '../../shared/activity-info-modal/activity-info-modal.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-assigned-activity-list',
  templateUrl: './assigned-activity-list.component.html',
  styleUrls: ['./assigned-activity-list.component.css']
})
export class AssignedActivityListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: AssignedActivityListDataSource ;
  loading:boolean;
/** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
displayedColumns = ['id', 'name', 'location','hostTeacher','hourCredit','semesterAndAcademicYear','students','info'];
activities: Activity[];
activitySize: number;
  filter: string;
  filter$: BehaviorSubject<string>;
  
  constructor(private dialog: MatDialog,private activityService: ActivityService, private authService: AuthenticationService,private teacherService: TeacherService, private router: Router) { }
  //Add data into the table
  addData() {
    this.loading = true;
    this.activityService.getActivityList()
    .subscribe(activities =>  
      setTimeout(
        () => {
          var filteredActivities = [];
          for (var i =0; i< activities.length;i++ ) {
            if ((activities[i] as any).host.id == this.authService.getCurrentUser().id) {
            filteredActivities.push(activities[i])
          }
        }
      this.dataSource = new AssignedActivityListDataSource();
      this.dataSource.data = filteredActivities;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;
      this.table.dataSource = this.dataSource;
      this.activities = filteredActivities;
      this.activitySize = filteredActivities.length;
      this.loading=false;
    }
      )
    ),0}
  ngOnInit() {
    
  }

  ngAfterViewInit() {
    this.addData();
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }
  routeToStudentListPage(actId: number) {
    this.router.navigate(['teacher/assignedactivityinfo',actId]);
  }
  openInformationModal(activity: any) {
    const dialogRef = this.dialog.open(ActivityInfoModalComponent, {
      width: '700px',
      data : { 
        name: activity.name,
        location:activity.location,
        description: activity.description,
        host: activity.host.name + ' ' +  activity.host.surname,
        maxEnrolledAmount: activity.maxEnrolledStudent,
        currentEnrolled: activity.enrolledStudents.length,
        hourCredit: activity.hourCredit,
        semester: activity.semester,
        academicYear: activity.academicYear,
      }
    });
    dialogRef.afterClosed().subscribe(confirm => { 
      // Don't do anything 
    });
  }
  
}
