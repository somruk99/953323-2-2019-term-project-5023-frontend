import Student from './student';
import Teacher from './teacher';
export default class Activity {
    id: number;
    name: string;
    location: string;
    description: string;
    hostTeacher: Teacher;
    hourCredit: number;
    maxEnrolledStudent: number;
    semester: number;
    academicYear: number;
    enrolledStudents: Student[]; 




}
