import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder } from '@angular/forms';
export interface ActivityData{
  name: string,
  location:string,
  description: string,
  host: string,
  maxEnrolledAmount: number,
  hourCredit:number,
  semester: number,
  academicYear: number,
  studentEnrolledHours: number,
  currentEnrolled: number
}
@Component({
  selector: 'app-activity-info-modal',
  templateUrl: './activity-info-modal.component.html',
  styleUrls: ['./activity-info-modal.component.css']
})
export class ActivityInfoModalComponent implements OnInit,AfterViewInit {
  
  constructor(
    public dialogRef: MatDialogRef<ActivityInfoModalComponent>, 
    private fb:FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public data: ActivityData
    ) {}

  form :any;
  

  ngOnInit(): void {
    //Set up the information preview
    this.setupForm();
  }
  ngAfterViewInit(): void {
  }
  setupForm() {
    this.form = this.fb.group({
      name: [''],
      location: [''],
      description: [''],
      host: [''],
      hourCredit: ['']
    });
  }
      
  goBack(): void { this.dialogRef.close(false); }

  submit() {
    setTimeout( ()=> 
      this.dialogRef.close(true)
      ,300); 
    }
}



