import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms'
import { AngularMaterialModule } from './angular-material';
import { StudentService } from './service/student.service';
import { ActivityService } from './service/activity.service';
import { TeacherService } from './service/teacher.service';
import { StudentImplService } from './service/student-impl.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptorService } from './helper/jwt-interceptor.service';
import { TeacherImplService } from './service/teacher-impl.service';
import { ActivityImplService } from './service/activity-impl.service';
import { HttpClientModule } from '@angular/common/http';
import { LoginPageComponent } from './auth/login-page/login-page.component';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { SystemRoutingModule } from './system-routing.module';
import {MatFileUploadModule} from "mat-file-upload";
import { SignupPageComponent } from './auth/signup-page/signup-page.component';
import { SignupConfirmModalComponent } from './auth/signupPage/signup-confirm-modal/signup-confirm-modal.component';
import { MatDatepickerModule, MatNativeDateModule, MatSortModule, MatTableModule, MatPaginatorModule } from '@angular/material';
import { StudentListComponent } from './admin/student-list/student-list.component';
import { ActivityListComponent } from './admin/activity-list/activity-list.component';
import { ActivityCreateComponent } from './admin/activity-create/activity-create.component';
import { AssignedActivityListComponent } from './teacher/assigned-activity-list/assigned-activity-list.component';
import { AssignedActivityStudentsComponent } from './teacher/assigned-activity-students/assigned-activity-students.component';
import { ProfileComponent } from './student/profile/profile.component';
import { ProfileEditComponent } from './student/profile-edit/profile-edit.component';
import { ConfirmModalComponent } from './student/profile-edit/confirm-modal/confirm-modal.component';
import { FindActivityComponent } from './student/find-activity/find-activity.component';
import { FindConfirmModalComponent } from './student/find-activity/find-confirm-modal/find-confirm-modal.component';
import { EnrolledActivityComponent } from './student/enrolled-activity/enrolled-activity.component';
import { WithdrawConfirmModalComponent } from './student/enrolled-activity/withdraw-confirm-modal/withdraw-confirm-modal.component';
import { ActivityInfoModalComponent } from './shared/activity-info-modal/activity-info-modal.component';
import { RejectConfirmModalComponent } from './teacher/assigned-activity-students/assigned-activity-students/reject-confirm-modal/reject-confirm-modal.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    NavComponent,
    SignupPageComponent,
    SignupConfirmModalComponent,
    StudentListComponent,
    ActivityListComponent,
    ActivityCreateComponent,
    AssignedActivityListComponent,
    AssignedActivityStudentsComponent,
    ProfileComponent,
    ProfileEditComponent,
    ConfirmModalComponent,
    FindActivityComponent,
    FindConfirmModalComponent,
    EnrolledActivityComponent,
    WithdrawConfirmModalComponent,
    ActivityInfoModalComponent,
    RejectConfirmModalComponent
  ],
  imports: [
    AngularMaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule ,
    FormsModule,
    HttpClientModule,
    LayoutModule,
    MatFileUploadModule,
    MatDatepickerModule,
    MatNativeDateModule ,
    MatSortModule, 
    MatTableModule, 
    MatPaginatorModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    SystemRoutingModule,
    AppRoutingModule
    
  ],
  providers: [
    { provide: StudentService, useClass: StudentImplService},
    { provide: ActivityService, useClass: ActivityImplService},
    { provide: TeacherService, useClass: TeacherImplService},
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent],
  entryComponents: [SignupConfirmModalComponent,ConfirmModalComponent,FindConfirmModalComponent,WithdrawConfirmModalComponent,ActivityInfoModalComponent,RejectConfirmModalComponent]
})
export class AppModule { }
