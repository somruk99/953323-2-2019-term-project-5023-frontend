import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { StudentService } from '../service/student.service';
import { AuthenticationService } from '../service/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  
  ngOnInit(): void { }

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  defaultImageUrl = 'assets/camt_icon.png';

constructor(private breakpointObserver: BreakpointObserver, 
  private authService: AuthenticationService,
  private router: Router) {}

logout() {
  this.router.navigate(['']);
  this.authService.logout();
}
hasRole(role: string) {
  return this.authService.hasRole(role);
}
  getRole(role:string) {
    if(this.hasRole('ADMIN')) {
      return "Admin";
    }
    if(this.hasRole('TEACHER')) {
      return "Teacher";
    }
    if(this.hasRole('STUDENT')) {
      return "Student";
    }
  }
get user() {
  return this.authService.getCurrentUser();
}
}
