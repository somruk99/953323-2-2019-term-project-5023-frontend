import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { StudentService } from '../../service/student.service';
import { FileUploadService } from '../../service/file-upload.service';
import { AuthenticationService } from '../../service/authentication.service';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { SignupConfirmModalComponent } from '../signupPage/signup-confirm-modal/signup-confirm-modal.component';
import { CommonService } from '../../common.service';

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.css']
})
export class SignupPageComponent implements OnInit {
  hide: boolean;
  progress: number;
  uploadedUrl: string;
  constructor(
    public dialog: MatDialog,
    private fb:FormBuilder,
    private router:Router,
    private studentService: StudentService,
    private fileUploadService:FileUploadService,
    private authenticationService:AuthenticationService) {}
  form = this.fb.group({
    id: [''],
    studentId: ['',Validators.compose([
      Validators.required,
      Validators.pattern('[0-9]{9}')
    ])],
    name: ['',Validators.compose([Validators.required,
      Validators.pattern('^.{2,30}$')])],
    surname: ['',Validators.compose([Validators.required,
      Validators.pattern('^.{2,30}$')])],
    dob: [''],
    image: [''],
    email: ['',Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
    password: ['',Validators.compose([Validators.required,
      Validators.pattern('^.{5,15}$')])],
    confirmPassword: ['',Validators.compose([Validators.required])]
  },{
    validator: this.matchPassword('password', 'confirmPassword')
})

  validation_messages = CommonService.profile_validation_messages;
    
  //Default image
  imgsrc: string = 'assets/images/person-icon.png';
  imgUploaded = false;
  minDate: Date =  new Date(1900,0,1);
  maxDate: Date =  new Date();
  //Preview the image link
  previewImage() {
    this.imgsrc = this.form.get('image').value;
  }

  matchPassword(password:string,confirmPassword: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[password];
      const matchingControl = formGroup.controls[confirmPassword];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
      }
      // set error on matchingControl if validation fails
      matchingControl.setErrors( control.value!==matchingControl.value? {mustMatch: true} : null);
    }
  }
  //Boolean to check for duplicate student id
  duplicateId: boolean = false;
  //Boolean to check for duplicate email
  duplicateEmail: boolean = false;
  //Show duplicate error
  showDupError: boolean = false;
  selectedImgFile: File;

  submit() {
          //Check for duplicate email in database
          this.authenticationService.checkDupEmail(this.form.value).subscribe(
            (dupEmail) => {
              this.duplicateEmail = (dupEmail.dup)? true:false;
            });
          //Check for duplicate student id in database
          this.authenticationService.checkDupStudentId(this.form.value).subscribe(
            (dupId) => {
              this.duplicateId = (dupId.dup)? true:false;
                //If either the email or student id is duplicated, then do not save
                if (this.duplicateEmail || this.duplicateId) {
                  this.showDupError = true;
                }
                else {
                  this.showDupError = false;
                  //Open to show profile confirm dialog and save
                  this.openProfileConfirmDialog()
                  //this.saveStudent();
                } 
                })
    }
          

  openProfileConfirmDialog() {
    const dialogRef = this.dialog.open(SignupConfirmModalComponent, {
      width: '600px',
      data : { 
        studentId: this.form.get('studentId').value,
        name: this.form.get('name').value,
        surname: this.form.get('surname').value,
        dob: this.form.get('dob').value,
        email: this.form.get('email').value,
        password: this.form.get('password').value,
        image: this.selectedImgFile,
      }
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.saveStudent();
      }
      else {
        //Do nothing;
      }
    });

  }
              
  //Preview image
  onSelectedFilesChanged(files?: FileList) {
    const uploadedFile = files.item(0);
    var fileReader  = new FileReader();
    fileReader.onload = function() {
      var output = document.getElementById('previewImg');
      output.setAttribute("src",fileReader.result.toString())
    }
    fileReader.readAsDataURL(uploadedFile);
    this.selectedImgFile = uploadedFile;
  }
  //Upload image
  onUploadClicked(files?: FileList) {
    console.log(typeof(files));
    console.log(files.item(0));
    const uploadedFile = files.item(0);
    this.progress = 0;
    this.fileUploadService.uploadFile(uploadedFile)
      .subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Sent:
            console.log('Request has been made!');
            break;
          case HttpEventType.ResponseHeader:
            console.log('Response header has been received!');
            break;
          case HttpEventType.UploadProgress:
            this.progress = Math.round(event.loaded / event.total * 100);
            console.log(`Uploaded! ${this.progress}%`);
            break;
          case HttpEventType.Response:
            console.log('User successfully created!', event.body);
            this.uploadedUrl = event.body;
            this.form.patchValue({
              image: this.uploadedUrl
            });
            this.previewImage();
            this.form.get('image').updateValueAndValidity();
            setTimeout(() => {
              this.progress = 0;
            }, 1500);
          }
        });
  }
  //Save + attach img to student
  saveStudent() {
    const uploadedFile = this.selectedImgFile;
    this.progress = 0;
    this.fileUploadService.uploadFile(uploadedFile)
      .subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Sent:
            console.log('Request has been made!');
            break;
          case HttpEventType.ResponseHeader:
            console.log('Response header has been received!');
            break;
          case HttpEventType.UploadProgress:
            this.progress = Math.round(event.loaded / event.total * 100);
            console.log(`Uploaded! ${this.progress}%`);
            break;
          case HttpEventType.Response:
            console.log('User successfully created!', event.body);
            this.uploadedUrl = event.body;
            this.form.patchValue({
              image: this.uploadedUrl
            });
            this.previewImage();
            console.log(this.form.value);
            this.form.get('image').updateValueAndValidity();
            setTimeout(() => {
              this.progress = 0;
            }, 1500);
            //Register the student into the database
            this.studentService.registerStudent(this.form.value).subscribe(
              (student) => {
              
                alert('Registration completed! plase log in into your new account!.');
                this.router.navigate(['../login']);
               
              }, (error)=> { alert('There was an error registering your information, please try later!');
            })
          }
        });
  }



  ngOnInit() {
  }

}

