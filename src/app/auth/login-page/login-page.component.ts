import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../service/authentication.service';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  authenticating:boolean;
   constructor(
     private authenService:AuthenticationService,
     private fb: FormBuilder,
     private router:Router,
     private route: ActivatedRoute
   ) { }
 
   form = this.fb.group({
    
     username: [''],
     password: ['']
   
   })
   returnUrl : string;
   isError = false;
   hide:boolean;
   fromHomePage: boolean;
   ngOnInit(): void { 
     // reset login status
     this.authenService.logout();
     this.isError = false;
     // get return url from route paramers or default to '/'
     this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
     this.fromHomePage = (this.returnUrl === '/')? true : false;
     console.log('return url ' + this.returnUrl);
   }
   
   submit() {
     this.authenticating = true;
     const value = this.form.value;
     this.authenService.login(value.username,value.password)
       .subscribe(
         data => {
           this.authenticating = false;
           console.log(data);
           this.isError = false;
               if (this.returnUrl == '/') {
                 this.loginRoute();
               }
               else {
                 this.router.navigate([this.returnUrl]);
               }
         },
         error => {
           this.authenticating = false;
           this.isError = true;
         }
       );
     console.log(this.form.value);
   }

   private loginRoute() {
    if (this.authenService.hasRole('TEACHER')) {
      this.router.navigate(['/teacher/assignedactivitylist']);
    }
    else if (this.authenService.hasRole('ADMIN')) {
      this.router.navigate(['/admin/activitylist']);
    }
    else if (this.authenService.hasRole('STUDENT')) {
      this.router.navigate(['/student/findactivity']);
    }
  } 

}


