import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder } from '@angular/forms';
export interface ProfileDialogData {
  studentId: string;
  name: string;
  surname: string;
  dob: Date;
  email: string;
  password: string;
  image: File;

}
@Component({
  selector: 'app-signup-confirm-modal',
  templateUrl: './signup-confirm-modal.component.html',
  styleUrls: ['./signup-confirm-modal.component.css']
})
export class SignupConfirmModalComponent implements OnInit,AfterViewInit {
  
  constructor(
    public dialogRef: MatDialogRef<SignupConfirmModalComponent>, 
    private fb:FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public data: ProfileDialogData
    ) {}

  form :any;
  imgsrc: string = 'assets/images/person-icon.png';
  minDate: Date;
  maxDate: Date;
  hide: boolean;
  ngOnInit(): void {
    //Set up the information preview
    this.setupForm();
  }
  ngAfterViewInit(): void {
    //Set the preview image
    this.setImage();
  }
  setupForm() {
    this.form = this.fb.group({
      studentId: [''],
      name: [''],
      surname: [''],
      dob: [''],
      email: [''],
      password: ['']
    });
  }

  setImage() {
    const imageFile = this.data.image;
    var fileReader = new FileReader();
    fileReader.onload = function() {
      var output = document.getElementById('preview');
      output.setAttribute("src",fileReader.result.toString())
    }
    fileReader.readAsDataURL(imageFile);
  }
    
  
  goBack(): void { this.dialogRef.close(false); }

  submit() {
    setTimeout( ()=> 
      this.dialogRef.close(true)
      ,300); 
    }
}



