import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginPageComponent } from './auth/login-page/login-page.component';
import { SignupPageComponent } from './auth/signup-page/signup-page.component';
import { AuthGuard } from './guard/auth-guard.service';
import { StudentListComponent } from './admin/student-list/student-list.component';
import { ActivityListComponent } from './admin/activity-list/activity-list.component';
import { ActivityCreateComponent } from './admin/activity-create/activity-create.component';
import { AssignedActivityListComponent } from './teacher/assigned-activity-list/assigned-activity-list.component';
import { AssignedActivityStudentsComponent } from './teacher/assigned-activity-students/assigned-activity-students.component';
import { ProfileComponent } from './student/profile/profile.component';
import { ProfileEditComponent } from './student/profile-edit/profile-edit.component';
import { FindActivityComponent } from './student/find-activity/find-activity.component';
import { EnrolledActivityComponent } from './student/enrolled-activity/enrolled-activity.component';



const systemRoutes: Routes = [
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: 'signup',
    component: SignupPageComponent
  },
  {
    path: 'admin/studentlist',
    component: StudentListComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ADMIN'] }
  },
  {
    path: 'admin/activitylist',
    component: ActivityListComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ADMIN'] }
  },
  {
    path: 'admin/activitycreate',
    component: ActivityCreateComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ADMIN'] }
  },
  {
    path: 'teacher/assignedactivitylist',
    component: AssignedActivityListComponent,
    canActivate: [AuthGuard],
    data: { roles: ['TEACHER'] }
  },
  {
    path: 'teacher/assignedactivityinfo/:id',
    component: AssignedActivityStudentsComponent,
    canActivate: [AuthGuard],
    data: { roles: ['TEACHER'] }
  },
  {
    path: 'student/findactivity',
    component: FindActivityComponent,
    canActivate: [AuthGuard],
    data: { roles: ['STUDENT'] }
  },
  {
    path: 'student/enrolledactivity',
    component: EnrolledActivityComponent,
    canActivate: [AuthGuard],
    data: { roles: ['STUDENT'] }
  },
  {
    path: 'student/profile',
    component: ProfileComponent,
    canActivate: [AuthGuard],
    data: { roles: ['STUDENT'] }
  },
  {
    path: 'student/profile/edit',
    component: ProfileEditComponent,
    canActivate: [AuthGuard],
    data: { roles: ['STUDENT'] }
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(systemRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class SystemRoutingModule {
}
